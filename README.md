# Soal-Shift-Sisop-Modul-4-C04-2022 

### Anggota Kelompok
* Meisya Salsabila Indrijo Putri  5025201114
* Muhammad Haikal Aria Sakti 05111940000088
* Marcelino Salim 5025201026

# Soal 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
### 1.A
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat `huruf besar` akan ter encode dengan `atbash cipher` dan `huruf kecil` akan terencode dengan `rot13`

Contoh : `“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”`

Untuk mengklasifikasikan apakah suatu direktori akan diencode atau didecode, kami membuat fungsi isAnimeku yang mengembalikan nilai true atau false. Ketika isAnimeku bernilai true, yaitu ketika terdapat substring `"Animeku_"`, maka direktori path yang dimasukkan akan diencode, sebaliknya ketika bernilai false maka akan di-decode.
* Fungsi isAnimeku:
```
    bool isAnimeku(const char *path) {
        for(int i=0;i<strlen(path)-8+1;i++)
            if(path[i] == 'A' && path[i+1] == 'n' && path[i+2] == 'i' && path[i+3] == 'm' && path[i+4] == 'e'
            && path[i+5] == 'k' && path[i+6] == 'u' && path[i+7] == '_') return 1;
        return 0;
    }
```
* Fungsi encode atbash+rot13:
```
    void encodeAtRot(char *s){
        for(int i=0;i<strlen(s);i++)
            if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
            else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+13)%26)+'a';
    }

    void decodeAtRot(char *s){
        for(int i=0;s[i];i++)
            if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
            else if(s[i]>='a'&&s[i]<110) s[i] = ((s[i]-'a'-13)+26)+'a';
            else if(s[i]>=110&&s[i]<='z') s[i] = ((s[i]-'a'-13)%26)+'a';
    }
```
### 1.B
Semua direktori di-rename dengan awalan `“Animeku_”`, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
* Fungsi fuse untuk `xmp_rename` akan dipanggil ketika terdapat suatu direktori atau file yang direname. Pada fungsi ini, kita bisa mengecek apakah direktori diubah menjadi direktori yang akan di-encode atau di-decode. 
* `fpath`  atau `tpath` akan di-encode ketika memiliki substring `"Animeku_"`. Jika tidak, direktori tersebut akan di-decode.
* Pada fungsi fuse `xmp_rename`:
```
    if (isAnimeku(fpath) && !isAnimeku(tpath)){
        printf("[Mendekode %s.]\n", fpath);
        sistemLog(fpath, tpath, 2);
        int itung = decodeFolderRekursif(fpath, 1000);
        printf("[Total file yang terdekode: %d]\n", itung);
    }
    else if (!isAnimeku(fpath) && isAnimeku(tpath)){
        printf("[Mengenkode %s.]\n", fpath);
        sistemLog(fpath, tpath, 1);
        int itung = encodeFolderRekursif(fpath, 1000);
        printf("[Total file yang terenkode: %d]\n", itung);
    }
```
### 1.C
Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode. (Penyeselesaian sama seperti 1B)
### 1.D
Setiap data yang terencode akan masuk dalam file `“Wibu.log”`

Contoh isi:

`RENAME terenkripsi /home/[USER]/Downloads/hai -->/home/[USER]/Downloads/Animeku_hebat`

`RENAME terdecode /home/[USER]/Downloads/Animeku_ -->/home/[USER]/Downloads/Coba`

Untuk menyelesaikan problem ini, kami membuat fungsi `logRename` yang terintegrasi dengan fungsi `sistemLog` yang dapat mencatat aktivitas rename dari suatu direktori. Fungsi ini nantinya akan dipanggil sebelum proses encode maupun decode dieksekusi.
* Fungsi `sistemLog`:
```
    void sistemLog(char *dir1, char *dir2, int tipe) {
        char buff[1024], cmd[32];
        if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logIngfo(cmd,buff);
        else{
            if(tipe == 3){ //mkdir
                strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logIngfo(cmd, buff);
            }else if(tipe == 4){ //rmdir
                strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
            }else if(tipe == 5){ //unlink
                strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
            }
        }    
    }
```
* Fungsi `logRename`:
```
    void logRename(char *cmd, int tipe, char *des){
        time_t t = time(NULL);
        struct tm* lt = localtime(&t);
        char waktu[30];
        strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
        char logNya[1100];
        sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
        FILE *out = fopen(fileLog, "a");
        fprintf(out, "%s\n", logNya);
        fclose(out);
        return;
    }
```
### 1.E
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Untuk menyelesaikan problem ini, kita akan menggunakan fungsi-fungsi dari library dirent.h. Kemudian, kita akan mengimplementasikan library dirent.h di fungsi `decodeFolderRekursif` maupun fungsi `encodeFolderRekursif` dengan nilai yang dikembalikan adalah total file yang berhasil di-encode ataupun di-decode.

Pada fungsi `encodeFolderRekursif` dan `decodeFolderRekursif` akan dilakukan proses scan file maupun folder di dalamnya. Proses decode maupun encode akan dilanjutkan pada suatu folder yang berhasil ditemukan berdasarkan parameter basePath yang dimasukkan saat pemanggilan fungsi.
* Fungsi `encodeFolderRekursif`:
```
    int encodeFolderRekursif(char *basePath, int depth) {
        char path[1000]; 
        struct dirent *dp; 
        DIR *dir = opendir(basePath);
        if (!dir) return 0;
        int itung=0;
        
        while((dp=readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
            strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
            struct stat path_stat;
            stat(path, &path_stat);
            if(!S_ISREG(path_stat.st_mode)&&depth>0)
                itung += encodeFolderRekursif(path, depth - 1),
                encodeFolder(basePath, dp->d_name);
            else if(encodeFile(basePath, dp->d_name) == 0) itung++;
        }
        closedir(dir);
        return itung;
    }
```
* Fungsi `decodeFolderRekursif`:
```
    int decodeFolderRekursif(char *basePath, int depth) {
        char path[1000];
        struct dirent *dp;
        DIR *dir = opendir(basePath);
        if(!dir) return 0;
        int itung = 0;
    
        while((dp = readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
            strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
            struct stat path_stat;
            stat(path, &path_stat);
            if(!S_ISREG(path_stat.st_mode) && depth>0)
                itung += decodeFolderRekursif(path, depth - 1),
                decodeFolder(basePath, dp->d_name);
            else if(decodeFile(basePath, dp->d_name) == 0) itung++;
        }
        closedir(dir);
        return itung;
    }
```
### Dokumentasi:
![nomer_1](/uploads/a17cac5012620735248c277f2176d36d/nomer_1.jpeg)
![wibu](/uploads/ffd866676a8c91c8d6bac40eb7536c64/wibu.jpeg)


# Soal 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut :
### 2.A
Jika suatu direktori dibuat dengan awalan `“IAN_[nama]”`, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma `Vigenere Cipher` dengan key `“INNUGANTENG”` (Case-sensitive, Vigenere).

Untuk mengklasifikasikan apakah suatu direktori akan diencode atau didecode, kami membuat fungsi isAnimeku yang mengembalikan nilai true atau false. Ketika isAnimeku bernilai true, yaitu ketika terdapat substring "IAN_", maka direktori path yang dimasukkan akan diencode, sebaliknya ketika bernilai false maka akan di-decode.
* Fungsi `isIAN`:
```
    bool isIAN(const char *path) {
        for(int i=0;i<strlen(path)-4+1;i++)
            if(path[i] == 'I' && path[i+1] == 'A' && path[i+2] == 'N' && path[i+3] == '_') return 1;
        return 0;
    }
```
* Fungsi Encode dan Decode `Vigenere Cipher` dengan key `“INNUGANTENG”` (case sensitive)
```
    void encodeVig(char *s) {
        char key[] = "INNUGANTENG";
        for (int i=0;s[i];i++)
            if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'+(key[i%((sizeof(key)-1))]-'A'))%26)+'A';
            else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+(key[i%((sizeof(key)-1))]-'A'))%26)+'a';
    }

    void decodeVig(char *s) {
        char key[] = "INNUGANTENG";
        for(int i=0;s[i];i++)
            if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'A';
            else if ('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'a';
    }
```
### 2.B
Jika suatu direktori di rename dengan `“IAN_[nama]”`, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
* Fungsi fuse `xmp_rename`:
```
    else if(isIAN(fpath) && !isIAN(tpath)){
        printf("[Mendekode %s.]\n", fpath);
        sistemLog(fpath, tpath, 2);
        int itung = decodeFolderRekursifIAN(fpath, 1000);
        printf("[Total file yang terdekode: %d]\n", itung);
    }else if(!isIAN(fpath) && isIAN(tpath)){
        printf("[Mengenkode %s.]\n", fpath);
        sistemLog(fpath, tpath, 1);
        int itung = encodeFolderRekursifIAN(fpath, 1000);
        printf("[Total file yang terenkode: %d]\n", itung);
    }
```
### 2.C
Apabila nama direktori dihilangkan `“IAN_”`, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya. (Penyeselesaian sama seperti 2B)
### 2.D
Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori `“/home/[user]/hayolongapain_[kelompok].log”`. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem. (Penyelesaian sekalian dengan 2E)
### 2.E
Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level `INFO` dan `WARNING`. Untuk log level `WARNING`, digunakan untuk mencatat syscall rmdir dan unlink.

Untuk menyelesaikan problem ini, kami membuat fungsi `logInfo` dan logWarning yang terintegrasi dengan fungsi `sistemLog` yang dapat mencatat aktivitas rename dari suatu direktori. Fungsi ini nantinya akan dipanggil sebelum proses encode maupun decode dieksekusi. Fungsi `sistemLog` menerima parameter `tipe`, dimana variable ini akan menentukan kategori dari sebuah system call. Di mana ketika tipe 3, maka termasuk INFO, dan ketika tipe 4 dan 5 akan termasuk warning dengan command masing-masing (RMDIR atau UNLINK).
```
    void sistemLog(char *dir1, char *dir2, int tipe) {
        char buff[1024], cmd[32];
        if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logIngfo(cmd,buff);
        else{
            if(tipe == 3){ //mkdir
                strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logIngfo(cmd, buff);
            }else if(tipe == 4){ //rmdir
                strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
            }else if(tipe == 5){ //unlink
                strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
            }
        } 
    }
```
* Fungsi `logIngfo`:
```
    void logRename(char *cmd, int tipe, char *des) {
        time_t t = time(NULL);
        struct tm* lt = localtime(&t);
        char waktu[30];
        strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
        char logNya[1100];
        sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
        FILE *out = fopen(fileLog, "a");
        fprintf(out, "%s\n", logNya);
        fclose(out);
        return;
    }
```
* Fungsi `logWarning`:
```
    void logWarning(char *cmd, char *des) {
        time_t t = time(NULL);
        struct tm* lt = localtime(&t); 
        char waktu[30];
        strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt); 
        char logNya[1100];
        sprintf(logNya, "WARNING::%s:%s::%s", waktu, cmd, des); 
        FILE *out = fopen(fileLogHayo, "a");
        fprintf(out, "%s\n", logNya);
        fclose(out);
    }
```
### Dokumentasi:
![nomer_2](/uploads/633fe00d0e43bfe98eda638f3a90a931/nomer_2.jpeg)
![hayolo](/uploads/a63954c3c044d13eec4238dc1ccf2680/hayolo.jpeg)

